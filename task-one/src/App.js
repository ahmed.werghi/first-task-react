import React, { Component } from 'react';
import './App.css';
import ValidationComponent from './TaskComponents/ValidationComponent.js';
import CharComponent from './TaskComponents/CharComponent.js';
class App extends Component {
  state=({valeur:0, ok:'text too short',text:"",
  listChars:[],yes:false
});
  textChangeHandler=(event)=>{
      this.setState({valeur:event.target.value.length,text:event.target.value});
      if(this.state.valeur<=5)
      {
        this.setState({ok:'text too short'});
      }
      else{
        this.setState({ok:'text long enough'});
      }
      const chars=[...this.state.listChars];
      console.log(chars);
      const tabChars=Array.from(this.state.text);
      console.log(tabChars);
      this.setState({listChars:tabChars});
  }
  deleteCharFromText=(index)=>{
    const listeChars1=[...this.state.listChars];
    var newText="";
    console.log(listeChars1);
    listeChars1.splice(index,1);
    console.log(listeChars1);
    listeChars1.forEach(elt=>
    { 
      newText+=elt;
    })
    console.log(newText);
    this.setState({text:newText,listChars:listeChars1,yes:true});
  }
  render() {
    let texts=null;
    texts=(
      <div>
        {
        this.state.listChars.map((elt,index)=>
        {
          return <CharComponent click={this.deleteCharFromText.bind(this,index)} valeur={elt}/>
        })
      }
      </div>
    )
    if(this.state.yes)
      {
        document.getElementById("go").value=this.state.text;
      }
    
    return (
      <div className="App">
        <h1>First Task one</h1>
        <input id="go" type="text" onChange={this.textChangeHandler}/>
        <ValidationComponent valeur={this.state.valeur} description={this.state.ok}/>
        {texts}
      </div>
    );
  }
}

export default App;
