import React from 'react';

const charComponent=(props)=>{
    const style={
        display : 'inline-block',
        padding : '16px',
        margin:'16 px',
        textAlign:'center',
        border: '1px solid black'
      }
    return <div style={style}
            onClick={props.click}>{props.valeur}
         </div>
}
export default charComponent;